<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}" >

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #666666;
                color: white;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }
            
            .wallpaper {
                width: 100%;
                height: 100vh;
                background-image: url("/images/home.jpg");
                background-repeat: no-repeat;
                background-size: cover;
            }

            .wallpaper img {
/*                display: block;
                width: auto;
                height: 100vh;*/
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                position: absolute;
                bottom: 20px;
                width: 100%;
                height: 20vh;
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

        </style>
    </head>
    <body>
        <div id="app" class="flex-center position-ref full-height">
            <div id="page_wallpaper" class="wallpaper">
                &nbsp;
                <!-- <img src="/images/home.jpg" id="page_wallpaper"> -->
            </div>
            <div class="content">
                <div class="title m-b-md">
                    This is a screen
                </div>
            </div>
        </div>
        <script src="/js/app.js" type="text/javascript"></script>
        <script type="text/javascript">

            var wallpaper = document.getElementById("page_wallpaper");

            Echo.channel('screen.1')
                .listen('screenBeam', (e) => {
                    //console.log(e.beam_contents.image);
                    //console.log(e);
                    wallpaper.style.backgroundImage = "url('/images/" 
                        + e.beam_contents.image 
                        + e.beam_contents.ext
                    + "')";
                });
        </script>
    </body>
</html>
