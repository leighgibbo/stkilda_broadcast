<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/beam/{image_url}', function ($image_url) {

	$beam_contents = array(
		'image' => $image_url,
		'ext' => '.jpg',
		'screen_id' => 1
	);

	//Fire off the screenBeam Event, with the beam contents defined above, passed through.
    event(new \App\Events\screenBeam($beam_contents));
    return view('beam1', ['beam_contents' => $beam_contents]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/screen', function() {
	return view('screen');
});